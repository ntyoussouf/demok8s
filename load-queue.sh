#! /bin/sh
# Créer une file d'attente de travaux appelet 'keygen'
curl -X PUT localhost:8080/memq/server/queues/keygen

# Créer 100 éléments et les charge dans la file 
 for i in work-item-{1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40}; do
    curl -X POST  localhost:8080/memq/server/queues/keygen/enqueue \
       -d "$i"
 done

